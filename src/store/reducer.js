import ACTION_TYPES from "./actionTypes";

let user = localStorage.getItem("currentUser")
  ? JSON.parse(localStorage.getItem("currentUser"))
  : "";

let token = localStorage.getItem("token") ? localStorage.getItem("token") : "";

export const initialState = {
  user: user || "",
  token: "" || token,
  loading: false,
  errorMessage: null,
};

export const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ACTION_TYPES.REQUEST_LOGIN:
      return {
        ...state,
        loading: true,
      };

    case ACTION_TYPES.LOGIN_SUCCESS:
      return {
        ...state,
        user: payload.user,
        token: payload.token,
        loading: false,
      };

    case ACTION_TYPES.LOGOUT:
      return {
        ...state,
        user: "",
        token: "",
      };

    case ACTION_TYPES.LOGIN_ERROR:
      return {
        ...state,
        loading: false,
        errorMessage: payload,
      };

    default:
      throw new Error(`Unhandled action type: ${type}`);
  }
};
