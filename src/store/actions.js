import ACTION_TYPES from "./actionTypes";

const BASE_URL = "https://secret-hamlet-03431.herokuapp.com";

export const loginUser = async (dispatch, payload) => {
  const requestConfig = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  };

  try {
    dispatch({ type: ACTION_TYPES.REQUEST_LOGIN });

    const response = await fetch(`${BASE_URL}/login`, requestConfig);
    const data = await response.json();

    if (data.user) {
      dispatch({
        type: ACTION_TYPES.LOGIN_SUCCESS,
        payload: { user: data.user, token: data.auth_token },
      });
      localStorage.setItem("currentUser", JSON.stringify(data.user));
      localStorage.setItem("token", data.auth_token);
      return data;
    }

    dispatch({ type: ACTION_TYPES.LOGIN_ERROR, payload: data.errors[0] });
    return;
  } catch (error) {
    dispatch({ type: ACTION_TYPES.LOGIN_ERROR, payload: error.message });
  }
};

export const logOut = async (dispatch) => {
  dispatch({ type: ACTION_TYPES.LOGOUT });
  localStorage.removeItem("currentUser");
  localStorage.removeItem("token");
};
