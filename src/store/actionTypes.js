export const ACTION_TYPES = {
  REQUEST_LOGIN: "REQUEST_LOGIN",
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  LOGOUT: "LOGOUT",
  LOGIN_ERROR: "LOGIN_ERROR",
};

// const REQUEST_LOGIN = (payload) => {
//   return { type: ACTION_TYPES.REQUEST_LOGIN, payload };
// };

// const LOGIN_SUCCESS = (payload) => {
//   return { type: ACTION_TYPES.LOGIN_SUCCESS, payload };
// };

// const LOGOUT = () => {
//   return { type: ACTION_TYPES.LOGOUT };
// };

// const LOGIN_ERROR = (payload) => {
//   return { type: ACTION_TYPES.LOGIN_ERROR, payload };
// };

// const actions = {
//   REQUEST_LOGIN,
//   LOGIN_SUCCESS,
//   LOGIN_ERROR,
//   LOGOUT,
// };

export default ACTION_TYPES;
