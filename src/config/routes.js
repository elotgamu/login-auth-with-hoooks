// import * as React from "react";
import Login from "../pages/Login";
import Dashboard from "../pages/Dashboard";
import NotFound from "../pages/NotFound";

const routes = [
  {
    path: "/",
    component: Login,
    exact: true,
    loginRequired: false,
  },
  {
    path: "/dashboard",
    component: Dashboard,
    loginRequired: true,
  },
  {
    path: "/*",
    component: NotFound,
    loginRequired: false,
  },
];

export default routes;
