import { useReducer } from "react";

import AuthContext from "../contexts/AuthContext";
import DispatchContext from "../contexts/DispatchContext";
import { reducer, initialState } from "../store/reducer";

/**
 * @typedef {import('react').ReactNode} Children
 */

/**
 *
 * Our state management
 * Will be provide state by enclosing component
 * into this custom provider
 * @param {Object}     props
 * @param {Children}   props.children
 */
const AuthProvider = ({ children }) => {
  const [auth, dispatch] = useReducer(reducer, initialState);

  return (
    <AuthContext.Provider value={auth}>
      <DispatchContext.Provider value={dispatch}>
        {children}
      </DispatchContext.Provider>
    </AuthContext.Provider>
  );
};

export default AuthProvider;
