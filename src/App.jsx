import * as React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";

import AuthProvider from "./providers/AuthProvider";
import routes from "./config/routes";
import AppRoute from "./components/AppRoute";

import "./App.css";

function App() {
  return (
    <AuthProvider>
      <Router>
        <Switch>
          {routes.map((route) => (
            <AppRoute
              key={route.path}
              path={route.path}
              component={route.component}
              exact={route.exact}
              isPrivate={route.loginRequired}
            />
          ))}
        </Switch>
      </Router>
    </AuthProvider>
  );
}

export default App;
