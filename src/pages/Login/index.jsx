import * as React from "react";
import { useState, useContext } from "react";
import { useHistory } from "react-router";

// import { useAuthDispatch, useAuth } from "../../contexts/context";
import AuthContext from "../../contexts/AuthContext";
import DispatchContext from "../../contexts/DispatchContext";
// import { loginUser } from "../../reducer/actions";
import { loginUser } from "../../store/actions";

import styles from "./styles.module.css";

function Login() {
  const [data, setData] = useState({ email: "", password: "" });
  const history = useHistory();
  const { loading, errorMessage } = useContext(AuthContext);
  const dispatch = useContext(DispatchContext);

  const handleChange = (e) => {
    const { value, name } = e.target;
    setData({
      ...data,
      [name]: value,
    });
  };

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      const response = await loginUser(dispatch, data);
      if (!response.user) return;
      history.push("/dashboard");
    } catch (error) {
      console.log("error", error.message);
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.formContainer}>
        <h1>Login Page</h1>

        {errorMessage ? <p className={styles.error}>{errorMessage}</p> : null}

        <form>
          <div className={styles.loginForm}>
            <div className={styles.loginFormItem}>
              <label htmlFor="email">Username</label>
              <input
                type="text"
                id="email"
                name="email"
                value={data.email}
                onChange={handleChange}
                required
              />
            </div>
            <div className={styles.loginFormItem}>
              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                name="password"
                value={data.password}
                onChange={handleChange}
                required
              />
            </div>
          </div>
          <button type="submit" onClick={handleLogin} disabled={loading}>
            login
          </button>
        </form>
      </div>
    </div>
  );
}

export default Login;
