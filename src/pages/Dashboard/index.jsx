import * as React from "react";
import { useContext } from "react";
import { useHistory } from "react-router-dom";

import AuthContext from "../../contexts/AuthContext";
import DispatchContext from "../../contexts/DispatchContext";
import { logOut } from "../../store/actions";

import styles from "./styles.module.css";

function Dashboard() {
  const history = useHistory();
  const { user } = useContext(AuthContext);
  const dispatch = useContext(DispatchContext);

  const handleLogout = () => {
    logOut(dispatch);
    history.push("/");
  };

  return (
    <div style={{ padding: 10 }}>
      <div className={styles.dashboardPage}>
        <h1>Dashboard</h1>
        <button
          type="button"
          className={styles.logoutBtn}
          onClick={handleLogout}
        >
          Logout
        </button>
      </div>
      {user ? <p>Welcome {user.email}</p> : null}
    </div>
  );
}

export default Dashboard;
