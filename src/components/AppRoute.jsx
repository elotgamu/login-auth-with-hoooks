import * as React from "react";
import { useContext } from "react";

import { Redirect, Route } from "react-router-dom";
import AuthContext from "../contexts/AuthContext";

const AppRoute = ({ component: Component, path, isPrivate, ...rest }) => {
  const { token } = useContext(AuthContext);

  const getComponent = (props) =>
    isPrivate && !token ? <Redirect to="/" /> : <Component {...props} />;

  return (
    <Route
      path={path}
      render={(props) => getComponent(props)}
      {...rest}
    ></Route>
  );
};

export default AppRoute;
